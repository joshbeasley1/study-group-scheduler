json.extract! timeslot, :id, :slot_number, :created_at, :updated_at
json.url timeslot_url(timeslot, format: :json)
